import torch
import torch.nn as nn
from torch.nn.parameter import Parameter
import torch.nn.functional as F
import numpy as np
class Diverter(nn.Module):
    def __init__(self, ctx_dim, mcm_dim):
        super(Diverter, self).__init__()
        self.linear_c = nn.Linear(ctx_dim, ctx_dim)
        self.linear_t = nn.Linear(ctx_dim, mcm_dim)

    def maxout(self, ctx):
        output = self.linear_c(ctx)
        output = torch.split(output, 2, -1)
        output = torch.stack(output,2)
        output = output.topk(k=1,dim=-1)[0]
        output = output.squeeze(-1)
        return output


    def forward(self, mcm, ctx):
        # mxl_c -> 1xmxl_c -> bxmxl_c
        b_s = ctx.size(0)
        n_m,l_m = mcm.size()
        mcm = mcm.unsqueeze(0).expand(b_s, n_m, l_m)
        # bx1xl_c
        # t = self.maxout(ctx)
        t = self.linear_c(ctx)
        # bx1xl_m x bxl_mxm -> bx1xm 
        t = self.linear_t(t)
        mcm = mcm.transpose(2,1)

        output = torch.bmm(t, mcm)

        return output


class MecmModel(nn.Module):
    def __init__(self, embedding_encoder, embedding_decoder, diverter, encoder, decoder, generator):
        super(MecmModel, self).__init__()
        self.embedding_encoder = embedding_encoder
        self.embedding_decoder = embedding_decoder
        self.encoder = encoder
        self.diverter = diverter
        self.ench2dech = nn.Linear(2*decoder.hidden_size,decoder.hidden_size)
        self.decoder = decoder
        self.generator = generator
        # self.mecm_params = Parameter(torch.randn(10,decoder.hidden_size),requires_grad=True)
        self.mecm_embedding = nn.Embedding(10,decoder.hidden_size)
        self.logsm = nn.LogSoftmax(-1)

    def forward(self, src_inputs, tgt_inputs, src_lengths):

        # Run wrods through encoder

        encoder_outputs, encoder_hidden = self.encode(src_inputs, src_lengths, None)
        mecm_params = self.mecm_embedding.weight
        mecm_logits = self.diverter(mecm_params,encoder_hidden.transpose(0,1))
        
        mecm_probs = F.softmax(mecm_logits,-1).squeeze()
        selected_mecms = []
        for p in mecm_probs.tolist():
            selected_mecm = np.random.choice(10, 1, p)
            selected_mecms.append(selected_mecm[0])
        selected_mecms = torch.LongTensor(selected_mecms)
        if src_inputs.is_cuda:
            selected_mecms = selected_mecms.cuda()
        
        # mecm_log_pros = self.logsm(mecm_logits)
        # max_mecm_log_pros, max_mecm_idx= mecm_log_pros.max(-1)

        # batch_mecm = self.mecm_params.unsqueeze(0).expand(max_mecm_idx.size(0),
        #                                 self.mecm_params.size(0),
        #                                 self.mecm_params.size(1))
        # batch_selected_mecms = \
        #     torch.index_select(mecm_params, 0, selected_mecms)
        batch_selected_mecms = self.mecm_embedding(selected_mecms)

        mecm_loss = F.cross_entropy(mecm_logits.view(-1,10),selected_mecms,size_average=False)

        decoder_init_hidden = self.init_decoder_state(encoder_hidden,batch_selected_mecms)

        decoder_outputs , decoder_hiddens,attn = self.decode(
                tgt_inputs, encoder_outputs, decoder_init_hidden
            )        
        # outputs = self.generator(decoder_outputs)

        return decoder_outputs, mecm_loss



    def encode(self, input, lengths=None, hidden=None):
        emb = self.embedding_encoder(input)
        encoder_outputs, encoder_hidden = self.encoder(emb, lengths, None)

        return encoder_outputs, encoder_hidden
    def init_decoder_state(self, state, mecm_param):
        mecm_param = mecm_param.unsqueeze(0)

        last_hidden = torch.cat([mecm_param,state], -1)
        last_hidden = self.ench2dech(last_hidden)
        # last_hidden = mecm_param+state
        # last_hidden = self.ench2dech(last_hidden)
        return last_hidden      
    def decode(self, input, context, state):
        # mecm_param = self.mecm_params[mecm_idx].unsqueeze(0).expand(input.size(1), self.mecm_params.size(1))
        

        # last_hidden = torch.cat([mecm_param,state], -1) 
        # last_hidden = self.ench2dech(last_hidden)
        # last_hidden = mecm_param+state
        # last_hidden = self.ench2dech(last_hidden)

        emb = self.embedding_decoder(input)
        decoder_outputs , decoder_hiddens, attn = self.decoder(
                emb, context, state
            )   
        return decoder_outputs, decoder_hiddens, attn

    def save_checkpoint(self, epoch, opt, filename):
        torch.save({'model_dict': self.state_dict(),
                    'opt': opt,
                    'epoch': epoch,
                    },
                   filename)

    def load_checkpoint(self, filename):   
        ckpt = torch.load(filename,map_location=lambda storage, loc: storage)
        self.load_state_dict(ckpt['model_dict'])
        epoch = ckpt['epoch']
        return epoch


class NMTModel(nn.Module):
    def __init__(self, enc_embedding, dec_embedding, encoder, decoder, generator):
        super(NMTModel, self).__init__()
        self.enc_embedding = enc_embedding
        self.dec_embedding = dec_embedding
        self.encoder = encoder
        self.decoder = decoder
        self.generator = generator

    def forward(self, src_inputs, tgt_inputs, src_lengths):

        # Run wrods through encoder

        enc_outputs, enc_hidden = self.encode(src_inputs, src_lengths, None)


        dec_init_hidden = self.init_decoder_state(enc_hidden, enc_outputs)
            
        dec_outputs , dec_hiddens, attn = self.decode(
                tgt_inputs, enc_outputs, dec_init_hidden
            )        

        return dec_outputs, attn



    def encode(self, input, lengths=None, hidden=None):
        emb = self.enc_embedding(input)
        enc_outputs, enc_hidden = self.encoder(emb, lengths, None)

        return enc_outputs, enc_hidden

    def init_decoder_state(self, enc_hidden, context):
        return enc_hidden

    def decode(self, input, context, state):
        emb = self.dec_embedding(input)
        dec_outputs , dec_hiddens, attn = self.decoder(
                emb, context, state
            )     

        return dec_outputs, dec_hiddens, attn
    
    def save_checkpoint(self, epoch, opt, filename):
        torch.save({'encoder_dict': self.encoder.state_dict(),
                    'decoder_dict': self.decoder.state_dict(),
                    'enc_embedding_dict': self.enc_embedding.state_dict(),
                    'dec_embedding_dict': self.dec_embedding.state_dict(),
                    'generator_dict': self.generator.state_dict(),
                    'opt': opt,
                    'epoch': epoch,
                    },
                   filename)

    def load_checkpoint(self, filename):   
        ckpt = torch.load(filename)
        self.enc_embedding.load_state_dict(ckpt['enc_embedding_dict'])
        self.dec_embedding.load_state_dict(ckpt['dec_embedding_dict'])
        self.encoder.load_state_dict(ckpt['encoder_dict'])
        self.decoder.load_state_dict(ckpt['decoder_dict'])
        self.generator.load_state_dict(ckpt['generator_dict'])
        epoch = ckpt['epoch']
        return epoch

class MoSGenerator(nn.Module):
    def __init__(self, n_experts, input_szie, output_size):
        super(MoSGenerator, self).__init__()
        self.input_szie = input_szie
        self.output_size = output_size
        self.n_experts = n_experts
        self.prior = nn.Linear(input_szie, n_experts, bias=False)
        self.latent = nn.Sequential(nn.Linear(input_szie, n_experts*input_szie), nn.Tanh())
        self.out_linear = nn.Linear(input_szie, output_size)
        self.softmax = nn.Softmax(-1)


    def forward(self, input):
        latent = self.latent(input)
        
        logits = self.out_linear(latent.view(-1, self.input_szie))
        # 960 ,5
        prior_logit = self.prior(input).contiguous().view(-1, self.n_experts)
        prior = self.softmax(prior_logit)
        prob = self.softmax(logits).view(-1, self.n_experts, self.output_size)

        prob = (prob * prior.unsqueeze(2).expand_as(prob)).sum(1)
        log_prob = torch.log(prob.add_(1e-8))

        return log_prob